import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import NavegadorPrincipal from './navegadores/NavegadorPrincipal';

export default function App() {
  return (
    <NavigationContainer>
      <NavegadorPrincipal />
    </NavigationContainer>
  );
}